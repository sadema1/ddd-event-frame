# ddd-event-frame
DDD and event sourcing framework for ddd use-case educational projects

## Build the jar
The local system must contain jdk 17

```shell
./mvnw clean install
```
