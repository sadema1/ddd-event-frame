package nl.kristalsoftware.ddd.domain.base.aggregate;

import nl.kristalsoftware.ddd.domain.base.event.EventStorePort;
import nl.kristalsoftware.ddd.domain.base.type.TinyUUIDType;

public interface AggregateEventProcessor<T extends BaseAggregateRoot, U extends TinyUUIDType> {
    EventStorePort<T,U> getEventStorePort();

}
