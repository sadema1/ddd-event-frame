package nl.kristalsoftware.ddd.domain.base.aggregate;

import lombok.Getter;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventLoading;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventSaving;
import nl.kristalsoftware.ddd.domain.base.event.EventStorePort;
import nl.kristalsoftware.ddd.domain.base.type.TinyUUIDType;
import nl.kristalsoftware.ddd.domain.base.view.ViewStoreDocumentPort;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.function.Function;

public abstract class AggregateRepository<R extends AggregateRepository, T extends BaseAggregateRoot<U>,U extends TinyUUIDType> implements BaseRepository<R,T> {

    private final boolean AGGREGATE_EXISTS = true;
    private final boolean AGGREGATE_DOES_NOT_EXISTS = false;

    @Getter
    private final T aggregate;
    private final EventStorePort<T,U> eventStorePort;
    @Getter
    private final ViewStoreDocumentPort<T> viewStoreDocumentPort;

    protected AggregateRepository(Function<Boolean,T> aggregateFactory, AggregateEventProcessor<T,U> aggregateEventProcessor, ViewStoreDocumentPort<T> viewStoreDocumentPort) {
        this.aggregate = aggregateFactory.apply(AGGREGATE_DOES_NOT_EXISTS);
        this.eventStorePort = aggregateEventProcessor.getEventStorePort();
        this.viewStoreDocumentPort = viewStoreDocumentPort;
    }

    protected AggregateRepository(U reference, Function<Boolean,T> aggregateFactory, AggregateEventProcessor<T,U> aggregateEventProcessor, ViewStoreDocumentPort<T> viewStoreDocumentPort) {
        this.eventStorePort = aggregateEventProcessor.getEventStorePort();
        this.viewStoreDocumentPort = viewStoreDocumentPort;
        if (eventStorePort.eventsExists(reference)) {
            this.aggregate = aggregateFactory.apply(AGGREGATE_EXISTS);
            loadDomainEvents(aggregate);
            viewStoreDocumentPort.createDocument(aggregate);
        }
        else {
            this.aggregate = aggregateFactory.apply(AGGREGATE_DOES_NOT_EXISTS);
        }
    }

    private void loadDomainEvents(T aggregate) {
        List<DomainEventLoading<T>> aggregateDomainEvents = findAllDomainEventsByReference(aggregate);
        loadDomainEvents(aggregateDomainEvents);
    }

    private List<DomainEventLoading<T>> findAllDomainEventsByReference(T aggregate) {
        return eventStorePort.findAllDomainEventsByReference(aggregate);
    }

    private void loadDomainEvents(List<DomainEventLoading<T>> aggregateDomainEvents) {
        aggregateDomainEvents.stream()
                .forEach(it -> it.load(getAggregate()));
    }

    @Transactional
    public boolean saveEvents(List<DomainEventSaving<R>> domainEventList) {
        if (!domainEventList.isEmpty()) {
            saveAllEvents(domainEventList);
            commitDocumentChanges();
            return true;
        }
        return false;
    }

    protected abstract R getDomainRepository();

    private void saveAllEvents(List<DomainEventSaving<R>> domainEventList) {
        domainEventList.stream().forEach(it -> {
            it.save(getDomainRepository());
        });
    }

    private void commitDocumentChanges() {
        viewStoreDocumentPort.saveDocument();
    }

}
