package nl.kristalsoftware.ddd.domain.base.event;

import nl.kristalsoftware.ddd.domain.base.aggregate.BaseAggregateRoot;
import nl.kristalsoftware.ddd.domain.base.type.TinyUUIDType;

import java.util.List;

public interface EventStorePort<T extends BaseAggregateRoot, U extends TinyUUIDType> {
    List<DomainEventLoading<T>> findAllDomainEventsByReference(T aggregate);

    boolean eventsExists(U reference);
}
