package nl.kristalsoftware.ddd.domain.base.type;

import lombok.Getter;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class TinySetType<T> extends TinyType<Set<T>> {

    @Getter
    private Set<T> assigned = new HashSet<>();
    @Getter
    private Set<T> unassigned = new HashSet<>();


    public TinySetType(Set<T> items) {
        super(items);
    }

    @Override
    public Boolean isEmpty() {
        return getValue().isEmpty();
    }

    public void addItem(T item) {
        getValue().add(item);
    }

    public void removeItem(T item) {
        getValue().remove(item);
    }

    public void setItems(Set<T> items) {
        assigned = getAddedItems(items);
        unassigned = getRemovedItems(items);
    }


    private Set<T> getAddedItems(Set<T> items) {
        return items.stream()
                .filter(it -> !super.getValue().contains(it))
                .collect(Collectors.toSet());
    }

    private Set<T> getRemovedItems(Set<T> items) {
        return this.getValue().stream()
                .filter(it -> !items.contains(it))
                .collect(Collectors.toSet());
    }

    public boolean itemsChanged() {
        return !(assigned.isEmpty() && unassigned.isEmpty());
    }
}
