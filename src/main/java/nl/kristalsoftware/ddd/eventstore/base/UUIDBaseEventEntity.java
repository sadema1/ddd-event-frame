package nl.kristalsoftware.ddd.eventstore.base;

import nl.kristalsoftware.ddd.domain.base.event.DomainEventLoading;

import java.time.LocalDateTime;
import java.util.UUID;

public interface UUIDBaseEventEntity<T extends DomainEventLoading> {

    Long getId();

    UUID getReference();

    String getDomainEventName();

    LocalDateTime getCreationDateTime();

    T getDomainEvent();
}
