package nl.kristalsoftware.ddd.eventstream.base.offsetmanagement;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.common.TopicPartition;
import org.springframework.kafka.listener.ConsumerAwareRebalanceListener;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Component
public class OffsetManager implements ConsumerAwareRebalanceListener {

    private final JpaTopicPartitionRepository topicPartitionRepository;

    @Override
    public void onPartitionsAssigned(Consumer<?, ?> consumer, Collection<TopicPartition> topicPartitions) {
        log.debug("Retreive the current offset of all partitions...");
        for (TopicPartition topicPartition : topicPartitions) {
            Optional<TopicPartitionData> topicPartitionDataOptional =
                    topicPartitionRepository.findOneByTopicNameAndPartitionNumber(topicPartition.topic(), topicPartition.partition());
            topicPartitionDataOptional.ifPresent(it -> {
                long offset = it.getOffset();
                log.debug("Offset of partition {} is: {}", it.getPartitionNumber());
                consumer.seek(topicPartition, offset);
            });
        }
    }

}
