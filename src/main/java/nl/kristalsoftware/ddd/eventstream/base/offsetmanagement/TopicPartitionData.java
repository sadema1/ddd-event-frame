package nl.kristalsoftware.ddd.eventstream.base.offsetmanagement;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@Entity
@Table(name = "topicpartition")
public class TopicPartitionData {

    @Setter(AccessLevel.NONE)
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TOPICPARTITION_SEQ")
    @SequenceGenerator(name="TOPICPARTITION_SEQ", sequenceName = "TOPICPARTITION_SEQ", allocationSize=1)
    private Long id;

    private String topicName;

    @Column(name = "partition_number")
    private Integer partitionNumber;

    @Setter
    @Column(name = "partition_offset")
    private Long offset;

    public static TopicPartitionData of(String topicName, Integer partition, Long offset) {
        return new TopicPartitionData(null, topicName, partition, offset);
    }

}
