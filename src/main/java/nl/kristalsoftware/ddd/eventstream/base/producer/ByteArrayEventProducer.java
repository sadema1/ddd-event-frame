package nl.kristalsoftware.ddd.eventstream.base.producer;

import io.micrometer.core.annotation.Timed;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Slf4j
@Component
public class ByteArrayEventProducer {

    @Timed
    public void produceEvent(
            KafkaTemplate<String, byte[]> kafkaTemplate,
            String topicname,
            String key,
            byte[] eventData) {

        try {
            SendResult<String, byte[]> sendResult = kafkaTemplate.send(
                    topicname,
                    key,
                    eventData).get(5, TimeUnit.SECONDS);
            RecordMetadata metaData = sendResult.getRecordMetadata();
            log.info("Received new metadata. \n" +
                    "Topic: " + metaData.topic() + "\n" +
                    "Partition: " + metaData.partition() + "\n" +
                    "Offset: " + metaData.offset() + "\n" +
                    "Timestamp: " + metaData.timestamp()
            );
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        } catch (TimeoutException e) {
            throw new RuntimeException(e);
        }
    }

}
