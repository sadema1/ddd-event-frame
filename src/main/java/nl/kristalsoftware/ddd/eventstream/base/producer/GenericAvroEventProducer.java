package nl.kristalsoftware.ddd.eventstream.base.producer;

import lombok.extern.slf4j.Slf4j;
import org.apache.avro.specific.SpecificRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Slf4j
@Component
public class GenericAvroEventProducer<T extends SpecificRecord> {

    public void produceEvent(
            KafkaTemplate<String, T> kafkaTemplate,
            String topicname,
            String key,
            T eventData) {

        CompletableFuture<SendResult<String, T>> future = kafkaTemplate.send(
                topicname,
                key,
                eventData);
        try {
            SendResult<String, T> sendResult = future.get();
            RecordMetadata metaData = sendResult.getRecordMetadata();
            log.info("Received new metadata. \n" +
                    "Topic: " + metaData.topic() + "\n" +
                    "Partition: " + metaData.partition() + "\n" +
                    "Offset: " + metaData.offset() + "\n" +
                    "Timestamp: " + metaData.timestamp()
            );
            log.info(sendResult.getProducerRecord().value().toString());
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

}
